// Generates a note and things

function TextEntered()
{
	var numvalue = document.getElementById("numbervalue");
	var binaryvalue = document.getElementById("binary");
	
	// Get velocity
	var vel = document.getElementById("velocity").value || "0";
	vel = parseInt(vel) || 0;
	
	// Get value
	var val = document.getElementById("note").value || "0";
	val = parseInt(val) || 0;
	
	// Get length
	var len = document.getElementById("notelen").value || "0";
	len = parseInt(len) || 0;
	
	// Combine them into a single number
	var finale = len;
	finale |= (val << 16);
	finale |= (vel << 24);
	
	numvalue.innerHTML = finale.toString();
	
	// Split it to binary
	var binString = finale.toString(2).padStart(32, "0");
	var binA = binString.slice(0, 8);
	var binB = binString.slice(8, 16);
	var binC = binString.slice(16, 32);

	binaryvalue.innerHTML = binA + " " + binB + " " + binC;
}
